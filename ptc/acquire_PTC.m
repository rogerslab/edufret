%% acquire_PTC.m - Photon Transfer Curve Script

% Purpose of this is to acquire data for photon transfer curves (PTC) to
% characterize cameras and allow comparing them. 
%
% A PTC is generally a log-log plot of camera pixel noise as a function of
% camera pixel value. To generate such a curve, stacks of images are taken
% at a given exposure time and illumination; average values and noise (std)
% are computed from the stack(s) and plotted. A couple notes: 1) it is 
% important to subtract off a non-illuminated background for a given
% exposure time. 2) Illumination does not need to be uniform- filling out a
% PTC, i.e. acquiring values and noise values for all possible pixel values
% can be achieved by even illumination at different exposure times, 
% spatially modulated illumination at 1 exposure time, or some combination. 
%
% Main sections: 1) Call Camera opens the selected camera using MM 2)stand-
% alone read noise calculation computes the read noise using dark frames 3)
% Offset imaging computes average dark frames for every exposure time 4)
% Take images is where the illuminated frames are acquired for the series
% of exposure times. 5-7) Save output, Reshape and Plot, Close the
% camera are self explanatory
%
% Note: re: fprm, so fixed-pattern noise is correlated between frames. This
% program has a mode that subtracts every other frame, fprm=1, removing
% fixed pattern noise. Usually we are not using the camera in such a way
% that this is an issue, or have encountered cameras that have much for 
% fixed pattern noise, but its behavior is similar to relative intensity 
% noise ,i.e. ~pixel val, so there may be situations where this is relevant
% to our work with cameras, because of light-sources that are noisier than
% the shot noise limit. 
%
% Note: this script contains logic to generate PTC data as a function of
% varying a camera parameter, see 'vary_index' loop. The functionality is
% commented out to make it a bit more user friendly, but can be reinstated
% and used to look at e.g. camera noise as a function of gain. 
%
% Requires: 
% camera_handler.m 
% works via Micromanager (MM), so camera has to have an adapter in MM for
% this to work, but this allows comparing many different cameras with the
% same program. 
%
% Outputs: 
% PTC plots, saves PTC data, see below for details.  
%
% Acknowledgments: 
% Code started by JT Postlewaite, fleshed out by zjs, with contributions by 
% Genevieve Kears.  An excellent reference book used when writing this 
% code is 'Photon Transfer' by James Janesick. 

% Zach Simmons 
% zjsimmons@wisc.edu
% 2019.10.14

% Posted under GNU ver 3 license: 

%    eduFRET
%    Copyright (C) 2019  RogersLab

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                          Call Camera
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%for vary_index=1:9 %uncomment to collect PTC as fn of a parameter
vary_index=1;
    
%handles=camera_handler('IDS');
%handles=camera_handler('Quantalux');
%handles=camera_handler('IDS');
%handles=camera_handler('ORCA');
%handles=camera_handler('FLIR');
handles=camera_handler('ROLERAXR');
%handles=camera_handler('RETIGA'); 

   %perform camera evaluation as function of a varying parameter..
    
% we can change camera parameters via the handle; the exact parameters that
% you have access to depends on the camera, but as an example, for
% ROLERAXR:
gain_string=num2str(vary_index*5);
propGain = handles.core.getProperty('Camera1', 'Gain')
%handles.core.setProperty('Camera1', 'Gain', '45');%Gain values range from 1 to 45
handles.core.setProperty('Camera1', 'Gain',gain_string);%Gain values range from 1 to 45
propGain = handles.core.getProperty('Camera1', 'Gain')
%}
      
% PTC acqusition parameters:
% nothing magical about these selections, seemed to work fine in practice
xSizeROI=200;
ySizeROI=200;

N=25; %sample size per exposure, i.e. number of images per exposure time

M=logspace(log10(.03),log10(2000),20); %exposure times

% log-spaced to fill out PTC. What parameters are optimal very much depends 
% on illumination, i.e. widely spatially modulated image will need fewer 
% exposure times to acquire all possible camera mean values

width=handles.core.getImageWidth();
height=handles.core.getImageHeight();   
datcube=zeros(2,size(M,2));
darkcube=zeros(size(M,2),xSizeROI,ySizeROI);

%fixed pattern removal mode:
fprm=1;

%if not correcting, then fprm=0;
% aside: wonder if there is any difference between independent pairs or 
%pairs where each has an image in common with the next?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                 Stand-alone read noise calculation 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This isn't part of the PTC calculation per se so let's leave this
% commented out by default. Uncomment for another approach to getting at
% read noise. 

%{
% Don't know why I didn't think of this earlier, looking at difference 
% of frames to get at read noise:

% hmm can we also get at dark current/dark accumulation? i.e. it seems like
% the read noise is higher after not reading out the camera for a while.
% very interesting.. we could look at this in a controlled way by waiting
% for a set amount of time between reading the camera. say 5 min? This may 
% allow getting at dark current information. 

exp_time=0;
handles.core.setExposure(0); % set exposure- hopefully this gives you 
% whatever the minimum exposure time is, probably a better way to do this

exp_time=handles.core.getExposure()%what are the units here?..

%start any old time, but wait 5 min between 10th and 11th images? 
for j=1:20%+fprm
    %take image:
    %if j==11
    %   disp('waiting 5 min')
    %   pause(60*5)
    %end
    fail_count=1;
    myflag = true;
    while myflag
        try
            handles.core.snapImage(); % take a shot
            myflag = false;
        catch
            disp(strcat('snapImage() failed, consecutive fail count = ',num2str(fail_count)))
            fail_count=fail_count+1;
        end
    end    
    I1 = typecast(handles.core.getImage(), 'uint16');
    I1 = double(I1);
    I1=reshape(I1,[width,height])';
  %  roiI=I1(round(size(I1,1)/2)-xSizeROI/2+1:round(size(I1,1)/2)+xSizeROI/2,round(size(I1,2)/2)-ySizeROI/2+1:round(size(I1,2)/2)+ySizeROI/2);
    bias_stack(j,:,:)=I1;  
end %number of frames

%quick calc read noise from 2 subsequent frames:
diff_frame=squeeze(bias_stack(1,:,:))-squeeze(bias_stack(2,:,:));
read_noise=std(diff_frame(:))/sqrt(2)

%save stack of bias frames:
bias_tag={'Note: this is a stack of frames with shortest exposure time we can manage for a given camera..used to calculate read noise, can add a 5min gap between 10th and 11th images'}
filename = strcat('rolera_gains',num2str(vary_index)) %name file something, input as string

if isfile(strcat(filename,'.mat'))
    disp('this file already exists. please choose a new name, or delete the old file with this name, and run the cell again')
else
    save(filename,'bias_stack','bias_tag')
end
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                          Offset (dark) imaging
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%tic
%note: the offset may be the same if you vary some parameter, in which case
%you would only need to take this once..
if vary_index==1

for i=1:size(M,2)
    % for i=1:1
    exptemp=zeros(N,xSizeROI,ySizeROI);
    
    %set exposure time:
    %viewing and setting the exposure time, unterminate to view:
    exp_time=handles.core.getExposure();%what are the units here?..ms?
    millisecs =M(i);
    handles.core.setExposure(millisecs); % set exposure
    exp_time=handles.core.getExposure();
    
    %show progress
    disp(strcat(num2str(i),'/',num2str(size(M,2)),' exp time=',num2str(exp_time)))
    
    for j=1:N%+fprm
        
        %take image:
        % note: have seen some snapImage() occasionally fail on some
        % cameras- seems to depend on exposure time, or being highly
        % saturated. This should catch that.
        fail_count=1;
        myflag = true;
        while myflag
            try
                handles.core.snapImage(); % take a shot
                myflag = false;
            catch
                disp(strcat('snapImage() failed, consecutive fail count = ',num2str(fail_count)))
                fail_count=fail_count+1;
            end
        end
        
        % note: this may need tweaking for different cameras. Cameras
        % with bit-depths neither 8 or 16 may require tweaking
        % to give correct embedding and image size at the same time.
        I1 = typecast(handles.core.getImage(), 'uint16');
        %I1 = typecast(handles.core.getImage(), 'uint8');      
        %handles.frame.hide;%?
        I1 = double(I1);
        I1=reshape(I1,[width,height])';     
        roiI=I1(round(size(I1,1)/2)-xSizeROI/2+1:round(size(I1,1)/2)+xSizeROI/2,round(size(I1,2)/2)-ySizeROI/2+1:round(size(I1,2)/2)+ySizeROI/2);
        exptemp(j,:,:)=roiI;
        
        % figure(45) %trouble-shooting figure options
        % imagesc(roiI)
        % histogram(roiI)
        % ylim([0 100])
        % xlim([0 100])
        % pause
        
    end %number of frames
   
    % save raw data for debugging:
    % save(strcat('raw_data_backround_',num2str(i)),'exptemp')
    
    darkcube(i,:,:)=mean(exptemp,1);
end %number of exposure times

end %if vary_index=1

% debug figure - plot blackground
%{
figure(49)
%M=tag{2,2};
bg_vector=mean(mean(darkcube,3),2)
%semilogx(M,20*1/64*mean(mean(darkcube,3),2))
semilogx(M,bg_vector)

%ylim([0 150])
hold on
xlabel('exposure time')
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                 Take PTC (illuminated) images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Remove camera covering and press any key to continue imaging \n')
if vary_index==1
    pause()
end

%tic
for i=1:size(M,2)
    exptemp=zeros(N,xSizeROI,ySizeROI);
    
    %view and set exposure time:
    exp_time=handles.core.getExposure();
    millisecs =M(i);
    handles.core.setExposure(millisecs); % set exposure
    exp_time=handles.core.getExposure();
    disp(strcat(num2str(i),'/',num2str(size(M,2)),' exp time=',num2str(exp_time)))
    
    for j=1:N+fprm              
        %take image:
        % note: have seen some snapImage() occasionally fail on some
        % cameras- seems to depend on exposure time, or being highly
        % saturated. This should catch that.
        fail_count=1;
        myflag = true;
        while myflag
            try
                handles.core.snapImage(); % take a shot
                myflag = false;
            catch
                disp(strcat('snapImage() failed, consecutive fail count = ',num2str(fail_count)))
                fail_count=fail_count+1;
            end
        end
                
        % note: this may need tweaking for different cameras. Cameras
        % with bit-depths neither 8 or 16 may require tweaking
        % to give correct embedding and image size at the same time.
        I1 = typecast(handles.core.getImage(), 'uint16');        
        %handles.frame.hide;%?
        I1 = double(I1);
        I1=reshape(I1,[width,height])';        
        roiI=I1(round(size(I1,1)/2)-xSizeROI/2+1:round(size(I1,1)/2)+xSizeROI/2,round(size(I1,2)/2)-ySizeROI/2+1:round(size(I1,2)/2)+ySizeROI/2);        
        exptemp(j,:,:)=roiI;        
    end
    
    % save raw data for debugging:
    % save(strcat('raw_data_illuminated_',num2str(i)),'exptemp')
       
    % hmm so a camera with an offset was giving pixel values of zero, this 
    % was a hack to get a curve out, but resulting data would be suspect.
    %exptemp(exptemp==0)=nan;
    
    %subtract offset for images at that exposure time
    exptemp2=exptemp(1:end-fprm,:,:)-darkcube(i,:,:);
    
    %add to signal cube
    mean_cube(i,:,:)=mean(exptemp2,1);
    %mean_cube(i,:,:)=nanmean(exptemp2,1);
    
    %add to noise cube (including FPN)
    std_cube(i,:,:)=std(exptemp2,1,1);
    %std_cube(i,:,:)=nanstd(exptemp2,1,1);
    
    %if doing fixed pattern noise removal, exptem2 is replaced by
    %differences for noise calculation
    if fprm
        exptemp_holder=exptemp(2:end,:,:);
        exptemp3=exptemp(1:end-1,:,:)-exptemp_holder;
        clear exptemp_holder;
        factor=sqrt(2);       
        %add to shot noise cube
        std_cube2(i,:,:)=std(exptemp3,1,1)/sqrt(2);
        %std_cube2(i,:,:)=nanstd(exptemp3,1,1)/sqrt(2);
    end    
    %toc
end

% debug figures - plot avg signal and noise as fn of exposure time
%{
figure(46)
semilogx(M,mean(mean(mean_cube,3),2))
title('avg signal')
xlabel('exposure time')

figure(47)
semilogx(M,mean(mean(std_cube,3),2))
title('avg noise')
xlabel('exposure time')
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                            Close camera:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

handles.core.unloadAllDevices(); % unload your device

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                            Save output:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%notes:
tag{1,1}='Description:';
tag{1,2}='testing clean code with rolera gain loop';

tag{2,1}='times';
tag{2,2}=M;

tag{3,1}='Addition notes:';
tag{3,2}='std_cube2 is the fpn corrected';

% By default, this does not save raw images as the large number generated
% when building up statistics and scanning over exposure times would
% generate quite large files. It saves an roi-size x number of exposure 
% time cube of: average dark frames (darkcube), average signal frames
% (mean_cube), noise frames (std_cube) and fpn-corrected noise frames
% (std_cube2). 

filename=strcat('clean_code_test_with_rolera_gain_loop_',num2str(vary_index));

if isfile(strcat(filename,'.mat'))
    disp('this file already exists. please choose a new name, or delete the old file with this name, and run the cell again')
else
    save(filename,'darkcube','mean_cube','std_cube','std_cube2','tag')
end

%save('ptc_saved_data','darkcube','mean_cube','std_cube','std_cube2','tag')
%version that also saves the read noise from difference frames:
%save('ptc_test_flir_brightness','darkcube','mean_cube','std_cube','std_cube2','tag','bias_stack','read_noise')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                          Reshape and Plot:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

M=tag{2,2};

%quick and dirty look by frame: 
%so this is a simple but poor way to process this data. In order for this 
%to be at all useful, the acquisitions would need to be very evenly 
%illuminated. Additionally, you have to take many exposure times, and you 
%will likely not see the read-noise end of the PTC as it'll be swamped by
%the frame average. However, this can be sufficient to note differences
%between reporting mean and median values of noise (particularly at low 
%values) as well as hints of fpn if present. 

mean_list=mean_cube(1:size(M,2),:);
std_list=std_cube(1:size(M,2),:);
std_list2=std_cube2(1:size(M,2),:);

% avg by frame..
avg_std_list=mean(std_list,2);
avg_mean_list=mean(mean_list,2);
median_std_list=median(std_list,2);
median_std_list2=median(std_list2,2);

figure(1)
loglog(avg_mean_list,avg_std_list,'-xk')
hold on
loglog(avg_mean_list,median_std_list,'-xb')
hold on
loglog(avg_mean_list,median_std_list2,'-xr')
title('simple PTC')
xlabel('signal in a frame')
ylabel('std (noise) in a frame')
legend('noise-avg in frame','noise-median in frame','fpn-corrected median')

%better: bin it
%This is better as you're sort of decoupling the shape of the curve from
%the number of points at a mean value or which frame they were acquired in.
%This allows filling out the low mean value end of the PTC. 

M_dim=size(M,2);
[no_frames,roi_side,roi_side2]=size(std_cube)

%reshape:
mean_list=mean_cube(1:M_dim,:);
%std_list=std_cube(1:M_dim,:);
std_list2=std_cube2(1:M_dim,:);

%need to bin in a better way.. turn the whole mess into a single column 
mean_vector=reshape(mean_list,M_dim*roi_side*roi_side,1);
std_vector2=reshape(std_list2,M_dim*roi_side*roi_side,1);

%sort, crude way
data_vector(:,1)=mean_vector;
data_vector(:,2)=std_vector2;
data_vector=sortrows(data_vector);
std_vector2=data_vector(:,2);
mean_vector=data_vector(:,1);
clear data_vector

%log spaced bins- how about 100 between 1/100 a count to max pixel val
N_bins=logspace(log10(.01),log10(2^16),100);

%counter=1;
for n=1:length(N_bins)-1
  means_binned(n)= mean(mean_vector(mean_vector>=N_bins(n)&mean_vector<N_bins(n+1))); 
  %stds from mean of stds in a bin
  %std_binned(n)= mean(std_vector2(mean_vector>=M_bins(n)&mean_vector<M_bins(n+1))); 
  %std from median of stds in a bin, maybe less susceptible to extreme
  %values?
  std_binned(n)= median(std_vector2(mean_vector>=N_bins(n)&mean_vector<N_bins(n+1)));  
  number_binned(n)=length(std_vector2(mean_vector>=N_bins(n)&mean_vector<N_bins(n+1)));
end

%kill the bins that have too few entries for quality control.. say < 10?
 std_binned(number_binned<10)=nan;
 means_binned(number_binned<10)=nan;

%get rid of nans so we can fit, normally this shouldn't be an issue
%std_binned=std_binned(~isnan(std_binned));
%means_binned=means_binned(~isnan(means_binned));

% how about let's make an SNR curve as well:
snr=means_binned./std_binned;
snr(snr>2^8)=nan;%this is to suppress artificially high SNR due to saturation
figure(3)
loglog(means_binned,snr,'-x b','Linewidth',1.5)
title('SNR as a fn of mean vals, w/ binning')
xlabel('pixel mean value')
ylabel('SNR')

%PTC curve, binned
figure(2)
loglog(means_binned,std_binned,'-x k','Linewidth',1.5)
title('binned PTC result (benefit: bins extend PTC to lower mean vals)')
hold on
%compare to simple average, not binned
avg_mean_list=mean(mean_list,2);
median_std_list2=median(std_list2,2); %this is with every-other mode
loglog(avg_mean_list,median_std_list2,'--r','Linewidth',2)%this corrects for fpn
legend('binned', 'not binned')
ylim([1 1000])
xlim([.01 65000])
xlabel('pixel mean value')
ylabel('pixel standard deviation')
hold off

%raw PTC points
figure(4)
how_many=1000;
loglog(mean_list(:,1:how_many),std_list2(:,1:how_many),'.b')
ylim([1 1000])
xlim([.01 75000])
title(strcat('raw PTC points:',num2str(how_many),' points'))
hold on
xlabel('pixel mean value')
ylabel('pixel standard deviation')

%end %vary_index for-loop end

