%% fit_PTC.m - Photon Transfer Curve fitting

% Purpose of this is to plot and fit data for photon transfer curves (PTC) 
% to extract cameras parameters. 
%
% Data is binned which allows for examination of the low mean pixel part of
% the PTC. This is useful as it can show the camera running into the read
% noise floor. Fitting is implemented as a weighted fit in linear space to 
% allow extracting the read noise. Fit also pulls out k, the e- per DN. See
% acquire_PTC.m for more info about PTC and how the data is acquired. 
%
% Outputs: 
% PTC plots, fits PTC  
%
% Ideas/to do: Since you might want to combine sets, say taken with 
% different illumination conditions to fill out a PTC, it would make
% sense to add the ability to accommodate that to this. e.g. make it into a 
% function that accepts a number of input files an then combines them all 
% together. 
%
% Acknowledgments: 
% An excellent reference for this subject is 'Photon Transfer' by Janesick. 
% Example Rolera camera data acquired by Genevieve Kearns. 
%
% Zach Simmons 
% zjsimmons@wisc.edu
% 2019.10.14

% Posted under GNU ver 3 license: 

%    eduFRET
%    Copyright (C) 2019  RogersLab

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                          Load a set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%example Qlux ptc dataset
bit_depth=16;
load('example_data_quantalux_small_set.mat');camera='Quantalux';

%Qimaging Rolera-XR example sets:
bit_depth=12;
load('example_data_rolera_gain1_small_set.mat');camera='Rolera-gain=1';
%load('example_data_rolera_gain25_small_set.mat');camera='Rolera-gain=25';

%include others?:
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                           Display set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Note: this section appears in acquire_PTC as well

M=tag{2,2};

%quick and dirty look by frame: 
%so this is a simple but poor way to process this data. In order for this 
%to be at all useful, the acquisitions would need to be very evenly 
%illuminated. Additionally, you have to take many exposure times, and you 
%will likely not see the read-noise end of the PTC as it'll be swamped by
%the frame average. However, this can be sufficient to note differences
%between reporting mean and median values of noise (particularly at low 
%values) as well as hints of fpn if present. 

mean_list=mean_cube(1:size(M,2),:);
std_list=std_cube(1:size(M,2),:);
std_list2=std_cube2(1:size(M,2),:);

% avg by frame..
avg_std_list=mean(std_list,2);
avg_mean_list=mean(mean_list,2);
median_std_list=median(std_list,2);
median_std_list2=median(std_list2,2);

figure(1)
loglog(avg_mean_list,avg_std_list,'-xk')
hold on
loglog(avg_mean_list,median_std_list,'-xb')
hold on
loglog(avg_mean_list,median_std_list2,'-xr')
title('simple PTC')
xlabel('signal in a frame')
ylabel('std (noise) in a frame')
legend('noise-avg in frame','noise-median in frame','fpn-corrected median')

%better: bin it
%This is better as you're sort of decoupling the shape of the curve from
%the number of points at a mean value or which frame they were acquired in.
%This allows filling out the low mean value end of the PTC. 

M_dim=size(M,2);
[no_frames,roi_side,roi_side2]=size(std_cube);

%reshape:
mean_list=mean_cube(1:M_dim,:);
%std_list=std_cube(1:M_dim,:);
std_list2=std_cube2(1:M_dim,:);

%need to bin in a better way.. turn the whole mess into a single column 
mean_vector=reshape(mean_list,M_dim*roi_side*roi_side,1);
std_vector2=reshape(std_list2,M_dim*roi_side*roi_side,1);

%sort, crude way
data_vector(:,1)=mean_vector;
data_vector(:,2)=std_vector2;
data_vector=sortrows(data_vector);
std_vector2=data_vector(:,2);
mean_vector=data_vector(:,1);
clear data_vector

%log spaced bins- how about 100 between 1/100 a count to max pixel val
N_bins=logspace(log10(.01),log10(2^16),100);

%counter=1;
for n=1:length(N_bins)-1
  means_binned(n)= mean(mean_vector(mean_vector>=N_bins(n)&mean_vector<N_bins(n+1))); 
  %stds from mean of stds in a bin
  %std_binned(n)= mean(std_vector2(mean_vector>=M_bins(n)&mean_vector<M_bins(n+1))); 
  %std from median of stds in a bin, maybe less susceptible to extreme
  %values?
  std_binned(n)= median(std_vector2(mean_vector>=N_bins(n)&mean_vector<N_bins(n+1)));  
  number_binned(n)=length(std_vector2(mean_vector>=N_bins(n)&mean_vector<N_bins(n+1)));
end

% kill the bins that have too few entries for quality control.. say < 10?
% many bins may have zero entries depending on how many bins you create and 
% the evenness of illumination at the top end and discretization 
% manifesting in image stack averages at the low end
 
 std_binned(number_binned<10)=nan;
 means_binned(number_binned<10)=nan;
 std_binned=std_binned(~isnan(std_binned));
 means_binned=means_binned(~isnan(means_binned));

% how about let's make an SNR curve as well:
snr=means_binned./std_binned;
snr(snr>2^8)=nan;%this is to suppress artificially high SNR due to saturation
figure(3)
loglog(means_binned,snr,'-x b','Linewidth',1.5)
title('SNR as a fn of mean vals, w/ binning')
xlabel('pixel mean value')
ylabel('SNR')

%PTC curve, binned
figure(2)
loglog(means_binned,std_binned,'-x k','Linewidth',1.5)
title('binned PTC result (benefit: bins extend PTC to lower mean vals)')
hold on
%compare to simple average, not binned
avg_mean_list=mean(mean_list,2);
median_std_list2=median(std_list2,2); %this is with every-other mode
loglog(avg_mean_list,median_std_list2,'--r','Linewidth',2)%this corrects for fpn
legend('binned', 'not binned')
ylim([1 1000])
xlim([.01 65000])
xlabel('pixel mean value')
ylabel('pixel standard deviation')
hold off

%raw PTC points
figure(4)
how_many=1000;
loglog(mean_list(:,1:how_many),std_list2(:,1:how_many),'.b')
ylim([1 1000])
xlim([.01 75000])
title(strcat('raw PTC points:',num2str(how_many),' points'))
hold on
xlabel('pixel mean value')
ylabel('pixel standard deviation')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                      Fit in linear space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Note: do a weighted fit in linear space rather than a log-space fit
%because it lets us easily include the read noise term. It's easy to
%observe the slope is 1/2 in log-log space to verify you're in the shot
%noise regime, but you can't easily extract the read noise. That's why it's
% done this way. 

%use binned for more data at small mean values
x_tofit=(means_binned');
y_tofit=(std_binned');

%k_list=x_tofit./(y_tofit.^2)
%figure,plot(k_list)

cn=fittype('sqrt(rn^2+x/k)',...
    'dependent',{'y'},'independent',{'x'},'coefficients',{'rn','k'});

options = fitoptions(cn);
options.Lower = [0 0];
options.Upper = [10 1000];
options.Upper = [1000 1000];
options.Startpoint = [1 1];

%note: weights derived from variances, which is convenient since we already
%have them, more or less, as part of this procedure. The weighted fit is a
%way to try and make the low-magnitude parts of the curve of comparable
%importance to the high-magnitude parts, otherwise the fit may work well
%for the peak, but suck for the low-magnitude part of the curve. Weights are
%also convenient for excluding the saturation point at the end, etc. 

%may have to play with different portions/options for different cameras:
weights=ones(length(x_tofit),1);
%weights.. by 1/var of our data? 
weights_var=1./std_binned'.^2;
weights=weights.*weights_var;
%if e.g. saturated points at the end: 
weights(end-5:end)=0;
%exclude other ill-behaved weights
weights(isnan(weights))=0;
weights(isinf(weights))=0;
options.Weights=weights;

[myfit,gof]=fit(x_tofit,y_tofit,cn,options)

figure(5)
plot(myfit,x_tofit,y_tofit)
set(gca, 'YScale', 'log')
set(gca, 'XScale', 'log')
hold on
xlabel('counts')
ylabel('noise- std')

%hmm this looks fairly good, but do the numbers out make sense?..check your
%camera datasheet
rn=myfit.rn;
k=myfit.k;
dr=20*log10((2^bit_depth)/rn);
rn_e=rn*k;

dim = [.2 .5 .3 .3];
str = {strcat('k=',num2str(k),' e/DN');strcat('read noise=',num2str(rn),' DN');...
    strcat('electron read noise=',num2str(rn*k));strcat('estimated dynamic range=',num2str(dr),' dB')};
annotation('textbox',dim,'String',str,'FitBoxToText','on');
title('Quantalux camera PTC')
title(strcat(camera,' camera PTC fit'))

legend('off')
%ylim([1 7000])
%xlim([.01 75000])
xlabel('pixel mean value')
ylabel('pixel standard deviation')
