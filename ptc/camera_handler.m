 function [handles] = camera_handler(camera_type)
 
% This code contains how to communicate with a camera via micromanager (MM). 
% The goal is for this to be about the minimal piece of code needed to aid 
% in  integrating imaging via micromanager into other programs. The 
% function returns a handle that you can use with MM functions to take 
% images as well as set properties for the camera.

% This will require micromanager and adapters for specific cameras. You 
% also have to install the drivers for a given camera. If you can get 
% your camera working in the micromanager GUI this ought to work. Look at
% the MM config file for your camera for "handles.core.loadDevice(..." info 
% for your camera. 

% Inputs: 
% camera_type   - string indicating camera selection
% Outputs:
% handles       - handle for camera

% Camera specific notes: 
% Note: code has been down-graded to MM 1.4 as Quantalux (as currently 
% supported) requires MM 1.4; used to be supported under MM 2.0. Also, 
% appears Quantalux and IDS may conflict as the Thorlabs TSI cam installs
% a different driver version from the most current IDS version. 

% Acknowledgements: 
% Originally based on portions of code from Automation2d.m by Andrew 
% Radosevich, modified to get it to work with MM 2.0 and to work with 
% different cameras. Also includes contributions from Genevieve Kearns

% Zach Simmons 
% zjsimmons@wisc.edu
% 2019.10.14

% Posted under GNU ver 3 license: 

%    eduFRET
%    Copyright (C) 2019  RogersLab

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                          Open Camera
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%addpath(genpath('C:\Program Files\Micro-Manager-2.0beta'))  
%addpath(genpath('C:\Program Files\Micro-Manager-2.0gamma'))  
addpath(genpath('C:\Program Files\Micro-Manager-1.4'))  

import mmcorej.* % load the micro-manager library
%import MMCOREJ.* % load the micro-manager library

handles.core = CMMCore; %instantiate a camera object
handles.core.reset;
handles.core.unloadAllDevices(); % unload your device

%camera options: ORCA, Quantalux, IDS, FLIR, ROLERAXR, add more

if strcmp(camera_type,'ORCA')
    handles.core.loadDevice('Camera1', 'HamamatsuHam', 'HamamatsuHam_DCAM'); % load your camera.
elseif strcmp(camera_type,'Quantalux')
    handles.core.loadDevice('Camera1', 'TSI', 'TSICam');%load your camera.
elseif strcmp(camera_type,'IDS')
%original recipe:
   % handles.core.loadDevice('Camera1', 'IDS_uEye_original', 'IDS uEye'); % load your camera.
%modified version that incorporates changes.., all other version of IDS MM
%adapter commented out
   %handles.core.loadDevice('Camera1', 'IDS_uEye', 'IDS uEye ver2hp'); % load your camera.
   % handles.core.loadDevice('Camera1', 'IDS_uEye_modified', 'IDS uEye ver2hp'); % load your camera.
   handles.core.loadDevice('Camera1', 'IDS_uEye', 'IDS uEye'); % load your camera.
elseif strcmp(camera_type,'FLIR')
    handles.core.loadDevice('Camera1', 'PointGrey','Grasshopper3 GS3-U3-41C6NIR_19044557'); % load your camera.
elseif strcmp(camera_type,'ROLERAXR')
    handles.core.loadDevice('Camera1', 'QCam','QCamera'); % load your camera.
elseif strcmp(camera_type,'RETIGA')
    handles.core.loadDevice('Camera1','PVCAM','Camera-1')
else    
    disp('problem with camera selction')
end

try
    %so this doesn't seem to work, it can execute, but subsequent calls
    %won't work..zjs
    %handles.core.initializeDevice('Camera1'); %initialize my device
    handles.core.initializeAllDevices();%hmm the more general call does work
catch
    errordlg('Camera Didn''t Initiate''')
    return;
end

% % % % check property names and values for camera
k = handles.core.getDevicePropertyNames('Camera1');
for i=0:k.size-1;
    prop(i+1) = k.get(i);
    val(i+1) = handles.core.getProperty('Camera1', prop(i+1));
    disp([char(prop(i+1)) ':  ' char(val(i+1))])
    %pause(.4)
end
disp('now change settings:')

% So this spits out a list of the properties you have access to via the MM
% adapter for a given camera. Some of these properties are then set
% manually below. User can also set the properties in their program using
% the handler provided by this function, can look to this code for guidance
% on the syntax, etc. 

% Note: different cameras have different properties available via the MM 
% adapter. Different cameras have different functionality, but this also 
% reflects what functionality has been implemented via the adapter. A 
% camera may for example have functionality that a user might want to 
% utilize but that is not implemented in the MM adapter. It is possible to 
% address this by modifying and recompiling the camera adapter to add
% functionality. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%               Some Hard-coded settings for some cameras
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Note: this is not very concise, but I think it's useful for debugging and
% to see the progression: i) view current setting ii) change setting iii) 
% check change. 

if strcmp(camera_type,'IDS')
%set frame rate:
propFrameRate = handles.core.getProperty('Camera1', 'Frame Rate')
handles.core.setProperty('Camera1', 'Frame Rate', '.5');
propFrameRate = handles.core.getProperty('Camera1', 'Frame Rate')

%set gain, usually off: 
propGain = handles.core.getProperty('Camera1', 'Gain')
handles.core.setProperty('Camera1', 'Gain', '0');
propGain = handles.core.getProperty('Camera1', 'Gain')

%set PixelType: 
propPixelType = handles.core.getProperty('Camera1','PixelType')
%handles.core.setProperty('Camera1','PixelType','8bit mono')
handles.core.setProperty('Camera1','PixelType','10bit mono')
%handles.core.setProperty('Camera1','PixelType','12bit mono')
%handles.core.setProperty('Camera1','PixelType','16bit mono')
propPixelType = handles.core.getProperty('Camera1','PixelType')

%set shutter mode: 
propShutterMode = handles.core.getProperty('Camera1', 'Sensor Shutter Mode')
%handles.core.setProperty('Camera1', 'Sensor Shutter Mode', '1- Rolling shutter');
handles.core.setProperty('Camera1', 'Sensor Shutter Mode', '2- Global shutter');
%handles.core.setProperty('Camera1', 'Sensor Shutter Mode', '64- Rolling shutter with global start');
%handles.core.setProperty('Camera1', 'Sensor Shutter Mode', '128- Global shutter (alternative timing)');
propShutterMode = handles.core.getProperty('Camera1', 'Sensor Shutter Mode')

%OK, let's set our new properties that we have access to: 
%note: don't have access to all of these properties w/ original adapter)
%set offset (black level), 255 for now: 
propBlackLevel = handles.core.getProperty('Camera1', 'Offset')
%handles.core.setProperty('Camera1', 'Offset', '255');
handles.core.setProperty('Camera1', 'Offset', '255');
propBlackLevel = handles.core.getProperty('Camera1', 'Offset')

%set log mode
propLogMode = handles.core.getProperty('Camera1', 'Sensor Log Mode')
%handles.core.setProperty('Camera1', 'Sensor Log Mode', '3- auto log mode');
handles.core.setProperty('Camera1', 'Sensor Log Mode', '1- log mode off');
propLogMode = handles.core.getProperty('Camera1', 'Sensor Log Mode')

%set hot pixel correction
propHPCMode = handles.core.getProperty('Camera1', 'Hotpixel correction (sensor)')
%handles.core.setProperty('Camera1', 'Hotpixel correction (sensor)', 'sensor hot pixel correction on');
handles.core.setProperty('Camera1', 'Hotpixel correction (sensor)', 'sensor hot pixel correction off');
propHPCMode = handles.core.getProperty('Camera1', 'Hotpixel correction (sensor)')

%hmm pixel clock? 
propPixelClock = handles.core.getProperty('Camera1', 'Pixel Clock')
handles.core.setProperty('Camera1', 'Pixel Clock', '7');
%handles.core.setProperty('Camera1', 'Hotpixel correction (sensor)', 'sensor hot pixel correction off');
propPixelClock = handles.core.getProperty('Camera1', 'Pixel Clock')

%Binning? 
propBinning = handles.core.getProperty('Camera1', 'Binning')
%no binning
%handles.core.setProperty('Camera1', 'Binning', '1x1 (0)');
%2x2
%handles.core.setProperty('Camera1', 'Binning', '2x2 (0x1)');
%Not sure what these other modes mean..they all seem to do the same thing
%handles.core.setProperty('Camera1', 'Binning', '2x2 (0x2)');
%handles.core.setProperty('Camera1', 'Binning', '2x2 (0x3)');
propBinning = handles.core.getProperty('Camera1', 'Binning')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(camera_type,'ORCA')
%set conversion factor coefficient 
propCFactor = handles.core.getProperty('Camera1', 'CONVERSION FACTOR COEFF')
handles.core.setProperty('Camera1', 'CONVERSION FACTOR COEFF', '1');
propCFactor = handles.core.getProperty('Camera1', 'CONVERSION FACTOR COEFF')

%set conversion factor offset
propCFOffset = handles.core.getProperty('Camera1', 'CONVERSION FACTOR OFFSET')
handles.core.setProperty('Camera1', 'CONVERSION FACTOR OFFSET', '0');
propCFOffset = handles.core.getProperty('Camera1', 'CONVERSION FACTOR OFFSET')

%hmm appears you can't write these vals? interesting, looks like these are
%fixed and writing them doesn't change anything.. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(camera_type,'Quantalux')
    disp('quantalux hello')
%set hotpixel correction.. 
propHPmode = handles.core.getProperty('Camera1', 'HotPixel')
handles.core.setProperty('Camera1', 'HotPixel', 'On');
propHPmode = handles.core.getProperty('Camera1', 'HotPixel')

propBinning = handles.core.getProperty('Camera1', 'Binning')
handles.core.setProperty('Camera1', 'Binning', '1');%set between 1 and 16
propBinning = handles.core.getProperty('Camera1', 'Binning')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(camera_type,'FLIR')
    disp('FLIR hello')
    %to do: any properties we want to set? 
    %appears 'Brightness' is important, it's the offset/black level,
    %however we don't have access to it via the MM adapter
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(camera_type,'ROLERAXR')
    disp('ROLERAXR hello')
    
propBinning = handles.core.getProperty('Camera1', 'Binning')
handles.core.setProperty('Camera1', 'Binning', '1');%set 1 2 4 8
propBinning = handles.core.getProperty('Camera1', 'Binning')

%set the bit depth as well, only 8 by default. 
propPixelType = handles.core.getProperty('Camera1', 'PixelType')
handles.core.setProperty('Camera1', 'PixelType', '12bit');%or '8bit'
propPixelType = handles.core.getProperty('Camera1', 'PixelType')

%to do: add gain functionality, look at as a fn of gain. 
propGain = handles.core.getProperty('Camera1', 'Gain')
handles.core.setProperty('Camera1', 'Gain', '45');%Gain values range from 1 to 45
propGain = handles.core.getProperty('Camera1', 'Gain')



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(camera_type,'RETIGA') 
    disp('Retiga Hello')

propBinning = handles.core.getProperty('Camera1', 'Binning')
handles.core.setProperty('Camera1', 'Binning', '1');%set 1 2 4 8 12 16
propBinning = handles.core.getProperty('Camera1', 'Binning')

%to do: add gain functionality, look at as a fn of gain. 
propGain = handles.core.getProperty('Camera1', 'Gain')
handles.core.setProperty('Camera1', 'Gain', '2');%Gain values range from 1 to 2
propGain = handles.core.getProperty('Camera1', 'Gain')

% add more cameras here:

end
 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%              Image acquisition logic for reference
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
% While everything above is camera specific, below is not. This demonstrates
% the part of the usefulness of communicating to a camera via MM this way.
% Once you have the adapter straightened out, you can use the same MM
% commands to talk to different cameras. Code below is included for reference
% if the user wants to make this into a standalone imaging program or
% incorporate elsewhere. 

%Take an image:

width=handles.core.getImageWidth();
height=handles.core.getImageHeight();
    
%set exposure time: 
%so here's functions for getting and setting the exposure time: 
exp_time=handles.core.getExposure()%what are the units here?..
millisecs =10 
%max for TL camera is ~2000ms
handles.core.setExposure(millisecs) % set exposure
exp_time=handles.core.getExposure()%what are the units here?..

%take image:
handles.core.snapImage(); % take a shot
I1 = typecast(handles.core.getImage(), 'uint16');
%handles.frame.hide;
I1 = double(I1);
I1=reshape(I1,[width,height])';
    
figure(12)
imagesc(I1)
axis image;axis off;

%%
%take images:
        max_reps=10
        reps=1;
        while reps <= max_reps
            handles.core.snapImage(); % take a shot
            temp = typecast(handles.core.getImage(), 'uint16');
            temp = double(temp);
            temp=reshape(temp,[width,height])';
            figure(12)
            imagesc(temp)
            title(num2str(reps))
            axis image;axis off;           
            % I1 = I1+temp;
            reps = reps+1;
        end

%%
%close the camera
handles.core.unloadAllDevices(); % unload your device
delete(instrfind)

%}